@title divina-dama
@author cartola
@genre samba
@key A
@group cavaco
@music
A.A#º.Bm.. E7..A.E.
F#m..Bm.. B7..E7..
Bm.E7.A.F#m. Bm7.C#7.F#m.F#7.
Bm.Dm.A.F#7. Bm.E7.A.E7.
@text
Tudo acabado e o baile encerrado
Atordoado fiquei
Eu dancei com você, divina dama
Com o coração queimando em chama
Fiquei louco Pasmado por completo
Quando me vi tão perto De quem tenho amizade
Na febre da dança Senti tamanha emoção
Devorar-me o coração Divina dama
Quando eu vi Que a festa estava encerrada
E não restava mais nada De felicidade
Vinguei-me nas cordas Da lira de um trovador
Condenando o teu amor Tudo acabado

@title sim
@author cartola
@genre samba
@key G
@group violao
@music
G..Cm6.Am7b5. G..Cm6.Am7b5. G.....
G7..C.. E..A7.D7
G.G7.C.F7. G.E.A7.D7. G.Bm.Am7.D7.
G.G#º.Am7.. D7..G..
G.G#º.Am.Cm6. G.D7.D#º.A7.D7.
@text
Sim,Deve haver o perdão Para mim
Senão nem sei qual será O meu fim

Para ter uma companheira Até promessas fiz
Consegui um grande amor Mas eu não fui feliz
E com raiva para os céus Os braços levantei Blasfemei
Hoje todos são co n tra mim

Todos erram neste mundo Não há exceção
Quando voltam a realidade Conseguem perdão
Porque é que eu Senhor Que errei pela vez primeira
Passo tantos dissabores E luto contra a humanidade inteira

@title coração-vulgar
@author paulinho-da-viola
@genre samba
@key C#m
@group cavaco,violao
@music
C#m7.... F#m7.C#°.G#7..
F#m7.... G#7..C#m7..

C#m7.B7.C°.C#m7. C#m7..D#7.G#7.
C#m7.B.E.. B7.A7.G#7..

C#m7..B.. C°..C#m7..
C#m7.B7.C#m7.. D#°..G#7..
@text
Morre mais um amor num coração vulgar
Deixa desilusão pra quem não sabe amar

E quem não sabe amar tem que sofrer
Porque não poderá compreender

Que o amor que morre é uma ilusão,
E uma ilusão deve morrer
Um verdadeiro amor nunca fenece
E pouca gente ainda o conhece

Meu bem, se o teu amor morrer,
É porque ninguém o entendeu
Deixa o teu coração viver em paz
O teu pecado foi querer amar demais.

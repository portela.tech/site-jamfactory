@title maria-madalena-da-portela
@author candeia
@genre samba
@key G
@group cavaco
@music
G.E7.Am.D7.
@text
Fui enganado meu bem por ela Maria Madalena da Portela
Tinha pele bronzeada, cheirava a flor de canela, o seu nome era
Leva-me um conto e quinhentos, essa moça de blusa amarela, que conversa a dela,
Mulher do sabor de mel, era uma jóia tão bela, tu sabes quem é
Para o anjo da guarda da nega eu acendi uma vela, zelei por ela
Ela me dava jiló, veja só, dizendo que era berinjela, aquela megera
Para agradar a crioula comprei um par de chinela, cor amarela
Mandei-lhe comprar presunto, ela trouxe mortadela, diz o nome da fera
No final de cinco anos prometi casar com ela, irmã de Estela
Era mãe de cinco filhos, metida a moça donzela, pois veja você
Ela de véu e grinalda fez promessa na capela, me traindo
Aniceto a conheceu, andava de olho nela, ele sabe quem é, diz
Eu entrava pela porta, eu fugia pela janela, quem me enganava
ESTA NEGRA É VADIA ESTA É MEGERA QUEM ME ENGANAVA

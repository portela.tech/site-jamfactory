@title meu-violão
@author paulinho-da-viola
@genre samba
@key C
@group cavaco
@music
C.A7.D7.. G7..C.. A7..D7.. G7..C..
Gm7.C7.F.. D7.G7.C..
A7..D7.. G7..C..
C7..F.. A7..Dm7.. Fm7..C.A7. D7.G7.C..
@text
Não posso passar sem meu violão Não posso viver sem carinho
Quando eu não tenho ninguém Corro e abraço o meu pinho

Com ele eu seguro a saudade E a tristeza de viver sozinho

Já vivi em minha vida Momentos de intensa paixão
E no fogo da ferida Eu até achava inspiração

Quantas vezes eu cantava Quando não podia nem falar
É que meu violão me ajudava A trazer a esperança dentro de um poema

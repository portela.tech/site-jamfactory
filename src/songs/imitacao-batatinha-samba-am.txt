@title imitacao
@author batatinha
@genre samba
@key Am
@group violao
@music
Bm7b5.E7.C#mb5.F#7. Bm7b5.E7/G#.E7.Am7.
Am.Bm7b5.E7.Am7. Dm7.G7.C7M.F7.
Bm7b5.E7.Am7..  Dm7.G7.C7M.F7.
F#º.E/G# F#º B7..E7..
Am7.Am/G.Bm7b5.E7.. Am7.Am/G.F7 Bm7/5- E7 Am7
@text
Ninguém sabe quem sou eu Também já não sei quem sou
Eu bem sei que o sofrimento De mim até se cansou
Na imitação da vida ninguém vai me superar
Pois sorrindo da tristeza Se não acerto chorar
Mesmo assim eu vou passando
Vou sofrendo e vou sonhando até quando despertar
Dona solução Reveja meu caso com atenção
A esperanaça que é forte Mora no meu coração

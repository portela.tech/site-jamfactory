@title grande-deus
@author cartola
@genre samba
@key E
@group cavaco
@music
E.F*.F#m.B7 E.F*.F#m.B7
Bm.E7.A.Dm,G7. C,A7.Dm,G7.C.B7.
F#m.B7.E/G#.G. F#m.B7.E/G#.G.
F#m.B7.D7.C#7. F#m.Am.E/G#.G.
F#m.B7.E/G#.F#m>B7
@text
Deus,Grande Deus
Meu destino bem sei Foi traçado pelos dedos
Deus Grande Deus
De joelhos aqui eu voltei para te implorar
Perdoai-me Sei que errei um dia
Oh! Perdoai-me pelo nome de Maria
Que nunca mais direi o que não devia
Eu errei, grande Deus Mas quem é que não erra
Quando vê seu castelo cair sobre a terra
Julguei Senhor, daquele sonho Eu jamais despertaria
Se errei, perdoai-me Pelo amor de Maria

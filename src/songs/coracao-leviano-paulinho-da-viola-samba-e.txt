@title coracao-leviano
@author paulinho-da-viola
@genre samba
@key E
@group cavaco,violao
@music
E.Eº.E..  G#m7.C#7.F#m7.C#7.
F#m7.D7M/F#.B7.. F#7..F#m7.B7.

A7M.A#º.E/B.C#7. F#7.B7.G#m7.Gm7.
F#m7.Gº.E/G#.C#7. F#7.B7.E..

F#m7.B7.E.Fº F#m7..A7.G#7.
C#m7.B7.E.C#7. F#7..F#m7.B7.

A7M.A#º.E/B.C#7.  A7.D7.C#7..
F#m7.Am7.G#m7.C#7. F#m7.B7.E..
@text
Trama em segredo teus planos Parte sem dizer adeus
Nem lembra dos meus desenganos Fere quem tudo perdeu

Ah!Coração leviano Não sabe o que fez do meu
Ah!Coração leviano Não sabe o que fez do meu

Este pobre navegante Meu coração amante Enfrentou a tempestade
No mar da paixão e da loucura Fruto da minha aventura Em busca da felicidade

Ah!Coração teu engano Foi esperar por um bem
De um coração leviano Que nunca será de ninguém

@title doxy
@author sonny-rollins
@genre jazz
@key Bb
@music
Bb7,Ab7.G7.C7,F7.Bb,F7#5.
Bb7,Ab7.G7.C7.F7.
Fm7.Bb7.Eb7.Edim.
Bb7,Ab7.G7.C7,F7.Bb,Cm7,F7.
@text

@title me-ta-kahu-kura
@author unknown
@genre maori
@key C
@music
A.Bm.E7.A.
@text
Mā te kahukura ka rere te manu – Mā ngā huruhuru nei
Ka rere koe – rere runga rawa e
Ka tae atu koe ki te taumata
Whakatau mai rā e

Mau ana taku aroha
Whai ake i ngā whetu
Rere tōtika rere pai
Rere runga rawa rā e


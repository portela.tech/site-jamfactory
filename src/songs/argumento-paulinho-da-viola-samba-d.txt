@title argumento
@author paulinho-da-viola
@genre samba
@key D
@group cavaco,repetorio
@music
D.A7.D.B7. Em.A7.D.A7.
Am7.D7.G6.G#º. F#m.B7.E7.A7.
@text
Tá legal, eu aceito o argumento
Mas não me altere o samba tanto assim
Olha que a rapaziada está sentindo a falta
De um cavaco de um pandeiro ou de um tamborim. Tá legal!

Sem preconceito ou mania de passado
Sem querer ficar do lado de quem não quer navegar
Faça como um velho marinheiro
Que durante o nevoeiro leva o barco devagar

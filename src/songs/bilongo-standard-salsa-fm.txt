@title bilongo
@author standard
@genre salsa
@key Fm
@music
Fm.Gb7.Fm.. C7.Db7.C7..
Bbm..C7.. Gm7b5.C7.Fm.C7.
Fm.Fm/Eb.Fm/Db.Fm/C. Fm.Eb9,Ab7.Db9,Gb9.C7,Db7.

C7..Fm.. C7..Fm..

Eb7.Ab.Gb.F9.Gb. Bbm.Eb7.Bbm.A7.Ebm.Ab13.D7.C7b9.
@text
yo estoy tan enamorado
de la negra Tomasa
que cuando se va de casa
qué triste me pongo

esa negra linda camara' me dio bilongo.
esa negra santa camara' me dio bilongo.

lo más que me gusta es la comida que ella cocina
lo más que me gusta es la café que ella me cuela

kikiribu mandinga

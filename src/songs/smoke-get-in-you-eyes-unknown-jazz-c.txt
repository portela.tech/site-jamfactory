@title smoke-get-in-you-eyes
@author unknown
@genre jazz
@key C
@music
Eb,Gb°.Fm,Bb7.Eb,G.Ab,Adim. Gm,Cm.Fm,Bb7.(Gm7,C7.Fm,Bb7.|Eb.Dbm,Gb7.)
B.E7.Ebm,Ab7.Dbm,Gb7. B.Fm,Bb7.Eb,C7.Fm,Bb7.
Eb,Gb°.Fm,Bb7.Eb,G.Ab,A°. Gm,Cm.Fm,Bb7.Eb,Cm7.Fm7,Bb7.
@text
They asked me how I knew My true love was true
I of course replied Something here inside cannot be denied
They said "someday you'll find all who love are blind" When your heart's on fire,
You must realize, smoke gets in your eyes

So I chaffed them and I gaily laughed To think they could doubt my love
Yet today my love has flown away, I am without my love (without my love)
Now laughing friends deride Tears I cannot hide
So I smile and say, When a lovely flame dies, smoke gets in your eyes

@title que-reste-t-il-de-nos-amours
@author charles-trénet
@genre french
@key Bbm
@music
F7M/A. G7b13.Em7.A7b13.  Dm7.G7b13.C7M.Fm6,C7M.
Cm.Db7M.Ab6.G. Cm.Dm7.C.G7.
F7M/A.G7.Em7.Gbº. Dm7.G7.(Am7.A7.|Gm6.Gb7.)
F7M Fm6 Em7 Bb7 A7 Dm7 D7 G7
F7M/A G7 Em7 Gbº Dm7 G7 C7M Fm6 C7M
Cm.Db7M.Ab6.G. Cm.Fm7.C.G7.
@text
Ce soir le vent qui frappe à ma porte
Me parle des amours mortes
Devant le feu qui s' éteint

Ce soir c'est une chanson d' automne
Dans la maison qui frissonne
Et je pense aux jours lointains

Que reste-t-il de nos amours
que reste-t-il de ces beaux jours
una photo vieille photo de ma jeunesse

que reste-t-il des billets doux
de mois d'avril des rendez vous
un souvenir qui me poursuit sans cesse

bonheur fanè cheveux au vent
baisers volès reves mouvant
que reste-t-il de tout cela dite le moi

un petit village un vieux clocher
un paysage si bien cachè
et dans un nuage le cher visage de mon passè

Les mots les mots tendres qu'on murmure
Les caresses les plus pures
Les serments au fond des bois

Les fleurs qu'on retrouve dans un livre
Dont le parfum vous enivre
Se sont envolés pourquoi?



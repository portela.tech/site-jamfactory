@title coisa-de-pele
@author jorge-aragão
@genre samba
@key G
@group cavaco,violao
@music
G..F#m7b5.B7. Em7..Dm7.G7.
C.Cm.G.E7. A7..Am.D7.

Gm7..D7.. Dm7b5.G7.Cm7..
F7..Bb.. A7.Am.D7.G.

G.D7.B7.Em. C.D7.G..
@text
Podemos sorrir Nada mais nos impede
Não dá pra fugir Dessa coisa de pele
Sentida por nós Desatando os nós
Sabemos agora. Nem tudo que é bom vem de fora

E a nossa canção pelas ruas e bares
Nos traz a razão Relembrando Palmares
Foi bom insistir Compôr e ouvir
Resiste quem pode na força dos nossos pagodes

E o samba se faz Prisioneiro pacato dos nossos tantãs
O banjo liberta Da garganta do povo as suas emoções
Alimentando muito mais A cabeça de um compositor
Eterno reduto de paz Nascente das várias feições do amor


Art popular do nosso chão
É o povo quem conduz o show E assina a direção


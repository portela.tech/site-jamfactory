@title nao-deixe-o-samba-morrer
@author alcione
@genre samba
@key Bm
@group cavaco
@music
Bm.C#m7b5.F#7
Bm.B7.Em.. C#m7b5.F#7.Bm.B7.
Em.A.D.G. C#m7b5.F#7.Bm.B7.
@text
Não deixe o samba morrer Não deixe o samba acabar
O morro foi feito de samba De samba pra gente sambar
Quando eu não puder pisar mais na avenida
Quando as minhas pernas não puderem aguentar
Levar meu corpo junto com meu samba
O meu anel de bamba entrego a quem mereça usar
Eu vou ficar no meio do povo espiando
Minha escola perdendo ou ganhando Mais um carnaval
Antes de me despedir Deixo ao sambista mais novo
O meu pedido final

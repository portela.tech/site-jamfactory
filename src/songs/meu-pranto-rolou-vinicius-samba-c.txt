@title meu-pranto-rolou
@author vinicius
@genre samba
@key C
@music
F.Em.A7.. D/F#..F..
C..Am.. D7..G7..

Dm.A7.Dm.G7. C.Dm.Em.A7.
D7..F.A7. D7..G7..
@text
Meu pranto rolou
Mais do que água
na cachoeira
Depois que ela me abandonou

Na minha vida tranqüila e a vida eu levava
E nessa felicidade a verdade eu não via
Ela vivia dizendo não vou te deixar
Ela queria fazer o meu pranto rolar

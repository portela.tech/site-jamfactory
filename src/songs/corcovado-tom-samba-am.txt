@title corcovado
@author tom
@genre samba
@key Am
@group violao
@music
Am6 G#º/13b Gm7 C7/9 F7M F6
Fm7 A#7/9 Em7 A7/13b D7/9 Dm7/9
A#7b9 Em7 Am7
Dm7 Gsus G7/9b E7/9# A7/13b : Dm7 Gsus G#º Am6
@text
Um cantinho, um violão Esse amor, uma canção
Pra fazer feliz a quem se ama
Muita calma pra pensar E ter tempo pra sonhar
Da janela vê-se o corcovado O Redentor, que lindo
Quero a vida mesmo assim Com você perto de mim
Até o apagar da velha chama
E eu que era triste Descrente desse mundo
Ao encontrar você eu conheci
O que é felicida - de, meu amor

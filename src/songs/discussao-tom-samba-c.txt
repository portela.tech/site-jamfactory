@title discussao
@author tom
@genre samba
@key C
@group violao
@music
C.Ebº.Dm.Ebº Em.E7.F.Fm.
Em.Ebº.Gm.A7. D7.Dm7.Ab6..
Em.Ebº.Dm.Ebº. Em.E7.F.Fm.
Em.Ebº.Gm.A7. D7.G.C.Db.
@text
Se você pretende sustentar a opinião
E discutir por discutir só prá ganhar a discussão
Eu lhe asseguro,pode crer,Que quando fala o coração
Às vezes é melhor perder do que ganhar, você vai ver
Já percebi a confusão, Você quer ver prevalescer
A opinião sobre a razão, Não pode ser, não pode ser
Prá que trocar o sim por não,Se o resultado é solidão
Em vez de amor, uma saudade,vai dizer quem tem razão

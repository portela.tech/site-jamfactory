@title pecado-capital
@author paulinho-da-viola
@genre samba
@key Bm
@group cavaco
@music
E7/G#.F#7.Bm7. F#m7.B7.Em.Em/G.
A7.A7/C#.D7M..  C#m7b5.F#7.F#m7b5.B7
Em.F#7.Bm7.Bm7/A G7M.F#7.Bm7.F#7.

B.D#m7.G#m7.. C#m7,..G#7..
D#7.G#m7.D#m7.A#7 D#m7.Dm7.C#m7.F#7.
B.G#7.C#m7.F#7. F#m7.B7.E.F#7.
@text
Dinheiro na mão é vendaval, é vendaval
Na vida de um sonhador, de um sonhador
Quanta gente aí se engana, e cai da cama
Com toda a ilusão que sonhou
E a grandeza se desfaz,
quando a solidão é mais Alguém já falou

Mas é preciso viver,
E viver, não é brincadeira não
Quando o jeito é se virar
cada um trata de si,Irmão desconhece irmão
Dinheiro na mão é vendaval
Dinheiro na mão é soluçãoE solidão

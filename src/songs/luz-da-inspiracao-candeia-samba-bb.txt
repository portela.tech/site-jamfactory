@title luz-da-inspiracao
@author candeia
@genre samba
@key D
@group cavaco
@music
D.B7.Em.A7. Em.A7.D..
D7..G.B7. E7..A7..|Em.A7.D.A7.)

Dbm5b7.Gb7.Bm.. E7..A7..
@text
Sinto-me em delírio luz da inspiração
Acordes músi..cais
Invadiram o meu ser, sem querer
Me elevam ao infinito da paz

Sinto-me vazio No ar a flutuar
Eu já nem sei quem sou
A mente se une a alma
A calma reflete o amor (bis 2)

Nos braços da inspiração
A vida transfomei de escravo pra rei
E o samba que criei Tão divino ficou
Agora sei quem sou

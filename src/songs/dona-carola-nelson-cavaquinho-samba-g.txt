@title dona-carola
@author nelson-cavaquinho
@genre samba
@key G
@group cavaco
@music
G.Am7.D7.G. Em7.Am7.D7.E7.
C7M.Cm6.Bm7.E7.  Am.D7.G..
Am.D7.G.. Am7..D7.B7.
E7..A7.. Am7.D7.G..
@text
Levantei-me da cama Sem poder
Até hoje ninguém Veio me ver
Fui amigo enquanto Eu tive dinheiro
Hoje eu não tenho companheiro

Hoje eles fogem de mim, mas não faz mal
Amigo é só pra levar meu capital
Se não fosse a dona Augusta e a dona Carola
Eu saia do hospital de camisola

@title trilha-do-amor
@author grupo-revelacao
@genre samba
@key F#m
@group cavaco
@music
F#m..Am.. G#m..G#m7b5.C#7.
A7M.B7.G#m7.G#7. C#m..G#m7b5.C#7.
@text
Fui lá na Bahia de São Salvador Peguei um Axé nos Encantos de lá
Voltei pro meu rio do meu redentor
pra ver meu amor que é lá de Irajá
to cheio de saudade do nosso calor
e tudo que eu quero é chegar e poder
te amar, te abraçar
Matar a vontade que tanto senti
sem o teu carinho não da pra ficar
Rezei para o Nosso senhor do Bonfim
E trouxe uma fita pra te abençoar
E tive a certeza que a gente é assim
Um longe do Outro não dá pra ficar
Não dá pra separar
Eu vou seguindo a trilha do amor Enquanto essa paixão me guiar
deixa o coração me levar deixa o coração me levar
eu vou seguindo a trilha do amo onde você estiver vou estar
sem você não da pra ficar sem você não da pra ficar

@title luz-negra
@author nelson-cavaquinho
@genre samba
@key Fm
@group cavaco
@music
Fm..Db7.. Fm7..Db7..
Gm7b5.C7.Db7.C7. Fm.Bbm7.G7.C7.
Fm7..Bbm7.. Eb7..Ab7M..
Ab6..G7.. C7....
@text
Sempre só Eu vivo procurando alguém
Que sofra como eu também E não consigo achar ninguém
Sempre só E a vida vai seguindo assim
Não tenho quem tem dó de mim Estou chegando ao fim
A luz negra de um destino cruel
Ilumina o teatro sem cor
Onde estou desempenhando o papel
De palhaço do amor

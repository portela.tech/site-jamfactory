@title Exaltação à Mangueira
@author Velha Guarda Da Mangueira
@genre samba
@key A
@music
F#m.C#7.F#m.E7. A.C#7.F#m.C#7.
F#m.C#7.F#m.A7. D7.C#7.F#m.E7.

A.E7.A.C#m5-/7,F#7. Bm.F#7.Bm.F#7.
Bm.... E7..A..
Em.A7.D.. Dm.E7.A.E7.

A.E7.A.F#7. Bm.E7.A.E7.

Bm..E7.. D.Dm.A7.G7.
F#7..Bm.. B7..E7..
@text
A Mangueira não morreu nem morrerá
Isso não acontecerá
Tem seu nome na história
Mangueira tu és um cenário coberto de glória

Mangueira teu cenário é uma beleza que a natureza crio ( ôoo )
no morro com seus barracões de zinco quando amanhece que explendor
todo mundo te conhece ao longe
pelo som dos seus tamborins e o rufar do seu tambor

chego ( õoo ) , a mangueira chego ( ôo )
chego ( ôoo ), a mangueira chego ( ôo )

Mangueira teu passado de glória está gravado na história
é verde e rosa a cor da sua bandeira
pra mostrar pra essa gente
que o samba é lá em mangueira


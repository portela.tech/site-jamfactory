@title e-preciso-perdoar
@author tom-jobim
@genre samba
@key Bm
@group violao
@music
Bm7/9....  Bm7/9....
G6..Gm6.. F#m7..E7..
Bm7/9....  Am...F#7
@text
Ah, madrugada já rompeu
você vai me abandonar
Eu sinto que o perdão você não mereceu
Eu quis a ilusão agora a dor sou eu
Pobre de quem entendeu que a beleza de amar é se dar
e só querendo pedir nunca soube o que é perder pra encontrar
eu sei... que é preciso perdoar
foi você quem me ensinou que um homem como eu
Que tem por que chorar
Só sabe o que é sofrer se o pranto se acabar

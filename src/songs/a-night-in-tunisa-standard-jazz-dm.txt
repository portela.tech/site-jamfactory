@title a-night-in-tunisa
@author standard
@genre jazz
@key Dm
@music
D#7.Dm.D#7.Dm. D#7.Dm.Em7b5.A7b5,Dm
Am7b5.D7b9.Gm.Gm7,C7 Gm7b5.C7b9.F6.Em7b5,A7b5
@text
The moon is the same moon above you
Aglow with it's cool evening light
But shining at night, in Tunisia
Never does it shine so bright

The stars are aglow in the heavens
But only the wise understand
That shining at night in Tunisia
They guide you through the desert sand

Words fail, to tell a tale
Too exotic to be told
Each night's a deeper night
In a world, ages old

The cares of the day seem to vanish
The ending of day brings release
Each wonderful night in Tunisia
Where the nights are filled with peace
Where the nights are filled with peace

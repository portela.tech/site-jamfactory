@title carinhoso
@author pixinguinha
@genre samba
@key G
@group cavaco,violao
@music
G.... G....
Bm.... Bm...B7.

Em.A7.D.G7 C.E7.Am7.A7.
D/C.G/B.D#7.D7. G....

Bm7..Em.. F#7..Bm7,A#7.
D/A.Bm7.E7/G#.A/G. D/F#.Fm7.Am/E.D7.

G.... F#...A#º.
Am7..D7.. G..D7..

G.B7/F#.Em.B7/F#. Em/G.G7/B.C.E/D.
Am/E.D#7.D7.G. E7.Am7.D7.G.
@text
Meu coração Não sei por quê
Bate feliz Quando te vê
E os meus olhos ficam sorrindo. E pelas ruas vão te seguindo.
Mas mesmo assim, foges de mim

Ah, se tu soubesses como eu sou tão carinhoso. E o muito e muito que te quero.
E como é sincero o meu amor. Eu sei que tu não fugirias mais de mim
Vem, vem, vem, vem sentir o
calor dos lábios meus À procura dos teus

Vem matar esta paixão Que me devora o coração
E só assim, então Serei feliz, bem feliz

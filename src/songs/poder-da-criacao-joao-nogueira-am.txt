@title Poder da Criação
@author João Nogueira
@genre samba
@key Am
@group cavaco
@music
Am7.... G7....
Am..F9.. F#º..E7..
Dm..Bm7b5.E7. Am7....
Dm/F..E7.. Am..C7.B7,E7.

Am7.... G7....
Am..F9.. F#º..E7..
Dm..Bm7b5.E7. Bb7..A7.
Dm/F..  F7.E7.Am.E7.

Dm..Bm7b5.E7. Am7....

Dm..Bm7b5.E7. Am7..C..
Bm7.E7.Bm7b5.E7. Am.Gm6.F7.E7.
Am.C7/G.F7.E7.
@text
Não
ninguém faz samba só porque prefere.
Força nenhuma no mundo interfere
Sobre o poder da criação

Não
não precisa se estar nem feliz, nem aflito
Nem se refugiar em lugar mais bonito
Em busca da inspiração

Não,
ela é uma luz que chega de repente
Com a rapidez de uma estrela cadente
Que acende a mente e o coração

É
faz pensar que existe uma força maior que nos guia
Que está no ar Vem no meio da noite ou no claro do dia
Chega a nos angustiar

E
o poeta se deixa levar por essa magia
E um verso vem vindo e vem vindo uma melodia
E o povo começa a cantar  lá laiá


@title o-mal-e-o-bem
@author nelson-cavaquinho
@genre samba
@key C
@group cavaco
@music
C.A.Dm.A. Dm.G.C.G. C.A.Dm.A. D7..G7..
C.A.Dm.A. Dm.G.C.G. C.A.Dm.A. D7.G.C.G.
Fm.Bb.A.. Dm.G.C.. Dm.Fm.Em.A. D7..G7..
@text
Nunca é tarde pra quem sabe esperar
O que se espera há de se alcançar
Eu plantei o bem e vou colher o que mereço
A felicidade deve ter meu endereço

Eu não sei porque tu falas mal de mim
Se eu tenho defeitos Deus me fez assim
Mas tenho certeza do que me convém
Entre o mal e o bem

Tu és a treva e eu sou a luz
Entre nós dois não pode haver a união
Eu tenho a fé que me conduz
Pra me livrar de quem deseja apunhalar meu coração

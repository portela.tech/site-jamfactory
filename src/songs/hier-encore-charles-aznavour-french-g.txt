@title hier-encore
@author charles-aznavour
@genre french
@key G
@music
Am.D7.G.C. (F#m7|Am7).B7.Em.B,E7.
@text
Hier encore, j'avais vingt ans
Je caressais le temps et jouais de la vie;
Comme on joue de l'amour / Et je vivais la nuit
Sans compter sur mes jours qui fuyaient dans le temps.

J'ai fait tant de projets qui sont restés en l'air.
J'ai fondé tant d'espoirs qui se sont envolés.
Que je reste perdu ne sachant où aller,
Les yeux cherchant le ciel mais le coeur mis en terre.

Hier encore j'avais vingt ans,
Je gaspillais le temps en croyant l'arrêter,
et pour le retenir, même le devancer,
Je n'ai fait que courir et me suis essouflé.

Ignorant le passé, conjuguant au futur,
Je précédais de moi toute conversation;
et donnais mon avis que je voulais le bon
Pour critiquer le monde avec désinvolture

Hier encore j'avais vingt ans,
Mais j'ai perdu mon temps à faire des folies,
Qui ne me laissent au fond rien de vraiment précis,
Que quelques rides au front et la peur de l'ennui.

Car mes amours sont mortes avant que d'exister,
Mes amis sont partis et ne reviendront pas;
Par ma faute j'ai fait le vide autour de moi
Et j'ai gaché ma vie et mes jeunes années.

Du meilleur et du pire en jettant le meilleur
J'ai figé mes sourires et j'ai glacé mes peurs;
Ou sont-ils à present, à present
mes vingts ans mes vingts ans

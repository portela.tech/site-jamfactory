@title SARANDONGA
@author Compay Segundo
@genre salsa
@key D
@music
Em.Bm.B�.Am Am.E7.VI-7.Am
D7.C.G. Am.D.G.F7.E7.Am E7.VI-7   Am
C.G.Cm.G.
D7.G.Am.D. G.C.D7.G.
B / E / V-5 C#7 / D / VI-3 C / C#�/ D7 / E7 / A7 / D7 / G
G.D.Em.Bm. C.G.A7.D.
C.E7.E7/B.E7. E7/B.Am.E7.Am.
C.G.D7.G. Cm.G.G.F7.E7.Am.
E7.Am.C.C#.G.C.D7.G.
@text
Por tu simb�lico nombre de Cecilia
tan supremo que es el genio musical.
Por tu simp�tico rostro de africana
canelado do se admiran los matices de un vergel�.

Y por tu talla de arabesca diosa indiana,
que es modelo de escultura del imperio terrenal,
ha surgido del alma y de la lira
del bardo que te canta como homenaje fiel

este cantar cadente, este arpegio armonioso
a la linda Cecilia bella y feliz mujer.

Las l�nguidas miradas de tus profundos ojos
que dicen los misterios del reino celestial.
Y el sensible detalle de amor provocativo
de tus eb�rneos senos y tu cuerpo gentil.

Yo no s� qu� provoca el conjunto armonioso,
tu belleza imperiosa y tu virtud femenil,
que me siento encantado y la mente inspirada de afecto y de ilusi�n.
Por ti Santa Cecilia la m�s primorosa mujer divina.

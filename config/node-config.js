module.exports = {
  dev:{
    apiHost : "localhost",
    apiPort : 8001,
    nodeHost : "localhost",
    nodePort : 3001,
    siteName : "Lesa Debug",
    app : {
        TitleShort : "Lesa's ISP",
        TitleLong : "Lesa's Telecommunication Services",
        AppLogo : "lesamoa.gif"
      }
  },
  prd:{
    apiHost : "localhost",
    apiPort : 8001,
    nodeHost : "localhost",
    nodePort : 3001,
    siteName : "Lesa's ISP Account Admin",
    app : {
      TitleShort : "Lesa's ISP",
      TitleLong : "Lesa's Telecommunication Services",
      AppLogo : "lesamoa.gif"
    }
  }
};

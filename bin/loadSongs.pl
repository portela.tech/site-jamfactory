#!/usr/bin/perl
use warnings;
use strict;
use File::Basename;
use JSON::PP;

system 'mkdir -p dist/json';
system 'mkdir -p dist/songs';
system 'mkdir -p dist/groups';
system 'rm dist/json/*';
system 'rm dist/groups/*';
system 'rm dist/songs/*';

my %keys  = ();
my %groups  = ();
my %genres  = ();
my $json  = JSON::PP->new->utf8->allow_nonref;
my @allSongs = () ;
my %titles ;
LOOP:
for my $f (`find src/songs -name \\*.txt | grep -v /draft`) {
    my $basename = basename($f);
    my ($rating,$url,$genre,$author,$key,$title,$text,$music,@groups);
    $rating = 3;
    $url= $genre = $author = $key = $title = $text = $music = "" ;
    open(IN,"<$f") || die "Couldn't open $f\n";
    chop $f;
    my $mode = "";
    while(<IN>) {
      if (m/^\s*\@genre\s+(.+)\s*$/o) {
        $genre = $1 ;
        $genre =~s/^\_//o ;
        $genres{$genre} = 1;
        $mode = "";
      } elsif (m/^\s*\@group\s+(.+)\s*$/o) {
        @groups = split(/,/,$1);
        for my $grp (@groups) {
           $groups{$grp} = 1;
        }
      } elsif (m/^\s*\@author\s+(.+)\s*$/o) {
        $author = $1 ;
        $author =~s/\s+/_/og ;
        $mode   = "";
      } elsif (m/^\s*\@key\s+(.+)\s*$/o) {
        $key = $1;
        $mode = "";
      } elsif (m/^\s*\@title\s+(.+)\s*$/o) {
        $title = $1;
        $mode = "";
      } elsif (m/^\s*\@url\s+(.+)\s*$/o) {
        $url = $1;
        $mode = "";
      } elsif (m/^\s*\@text\s*$/o) {
        $mode = "text";
      } elsif (m/^\s*\@music\s*$/o) {
        $mode = "music";
      } elsif (m/^\s*\@(\S+)\s*$/o) {
        print STDERR "Invalid tag in $basename : $1\n";
      } else {
      if ($mode eq "text") {
        $text .= $_ ;
      } elsif  ($mode eq "music") {
        $music .= $_ ;
      }
    }
  }
  if (!$text) {
    $text = "";
  }
  close(IN);
  chop $basename;
  my $msg=""; my $err="";
  if (!$title) { $msg.=" !title";$err=$basename }
  if (!$author) { $msg.=" !author";$err=$basename }
  if (!$genre) { $msg.=" !genre";$err=$basename }
  if (!$key) { $msg.=" !key";$err=$basename }
  if (!($music=~/\S/o)) { $msg.=" !music";$err=$basename }
  $title = lc($title);
  $title =~ s/ /-/og;
  $author = lc($author);
  $key=~s/A#/Bb/o;$key=~s/Db/C#/o;$key=~s/D#/Eb/o;$key=~s/Gb/F#/o;$key=~s/G#/Ab/o;
  my ($keyTone,$keyMode) = ($key=~m/^(A|Bb|B|C|C#|D|Eb|E|F|F#|G|Ab)(m?)$/o);
  if (!$keyTone) {
    die "no keyTone : $key,$keyTone,$basename\n";
  }
  if ( !$keyMode ) {
    $keyMode = "maj";
  } else {
    $keyMode = "min";
  }
  my $theKey = "${keyTone}${keyMode}";
  $keys{$theKey} = 1;
  if ( ! ($genre=~m/^(samba|salsa|soul|jazz|theory|maori|french)$/io)  ) {
   $msg.=" genre invalid : ".$genre ; $err=$basename  ;
  }
  $title =~ s/_/-/og;
  $author =~ s/_/-/og;

  my $fname = lc("$title-$author-$genre-$key");
  if ( !$keyTone ) {
   $msg .= "key invalid: $key" ;
   $err = $1 ;
 } elsif ($key =~ m/^(A|B|C#|D|E|F#|G)$/) {
    $music =~ s/Gb/F#/og;
    $music =~ s/Ab/G#/og;
    $music =~ s/Bb/A#/og;
    $music =~ s/Db/C#/og;
    $music =~ s/Eb/D#/og;
  } elsif ($keyTone =~ m/^(Bb|Eb|F|Ab)$/) {
    $music =~ s/F#/Gb/og;
    $music =~ s/G#/Ab/og;
    $music =~ s/A#/Bb/og;
    $music =~ s/C#/Db/og;
    $music =~ s/D#/Eb/og;
  }
  if ($err) {
    die "ERROR $basename : $msg\n";
    #next LOOP ;
  } else {
    print "SUCCESS $fname $msg\n"
  }
  my $musicOneLine = $music ;
  $musicOneLine =~ s/\s+/ /og;
  $musicOneLine =~ s/\n/ /og;

  my @text = split( /\n\n/ , $text );
  my @music = split( /\n\n/ , $music );
  my @musicLineText = ($musicOneLine);
  push(@musicLineText,@text);
  my @myMusicLineText = grep { /\S/ } @musicLineText;
  my %songHash = (
      title => $title ,
      author => $author ,
      genre => $genre ,
      keyTone => $keyTone ,
      keyMode => $keyMode ,
      key => "${keyTone}${keyMode}",
      url => $url ,
      groups => \@groups,
      musicLine => ["$musicOneLine\n"],
      music => \@music,
      text => \@text,
      musicLineText => \@myMusicLineText,
  );
  push(@allSongs, \%songHash);
  if ($titles{$fname} ) {
    # die "Duplicate output filename : ".$fname;
  } else {
    $titles{$fname} = 1 ;
  }
  ### KINDLE ###
  my $jsonOut = $json->encode(\%songHash);
  $jsonOut =~ s/\n/ /og;
  open(JSON, ">dist/json/${fname}.json") || die;
  print JSON $jsonOut;
  close(JSON);
}

sub group2md($;$;$;$@) {
    my ($name,$type,$songs) = @_ ;
    open(MD,">dist/groups/$name.txt") || die;
    for my $s (@$songs) {
        print MD "## $s->{title} $s->{key} $s->{author}\n";
        for my $content (@{$s->{$type}}) {
            $content=~s/[\n]+$//o;
            print MD "$content\n";
        }
        print MD "\n";
    }
    close(MD) or die;
}

sub song2md($;$@) {
    my ($s) = @_ ;
    open(MD,">dist/songs/$s->{title}-$s->{author}-$s->{genre}.txt") || die;
    print MD "## $s->{title} $s->{key} $s->{author}\n";
    for my $content (@{$s->{musicLineText}}) {
        print MD "$content\n";
    }
    close(MD) or die;
}


foreach my $song (@allSongs) {
    song2md( $song );
}
foreach my $key (keys(%keys)) {
    my @list = grep { $key eq $_->{key} } @allSongs ;
    my @sorted = sort { $b->{author} cmp $a->{author} } @list;
    group2md("key-${key}-text","musicLineText",\@sorted);
    group2md("key-${key}-music","musicLine",\@sorted);
}
foreach my $group (keys(%groups)) {
    my @list = grep { grep {$_ eq $group} @{$_->{groups}} } @allSongs ;
    my @sorted = sort { $b->{key} cmp $a->{key} } @list;
    group2md("group-${group}-text","musicLineText",\@sorted);
    group2md("group-${group}-music","musicLine",\@sorted);
}
foreach my $genre (keys(%genres)) {
    my @list = grep { $_->{genre} eq $genre } @allSongs ;
    my @sorted = sort { $b->{key} cmp $a->{key} } @list;
    group2md("genre-${genre}-text","musicLineText",\@sorted);
    group2md("genre-${genre}-music","musicLine",\@sorted);
}


